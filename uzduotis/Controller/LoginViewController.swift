//
//  ViewController.swift
//  uzduotis
//
//  Created by Laurynas Kapacinskas on 2020-08-12.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class LoginViewController: UIViewController {
    
    var password = String()
    var userName = String()
    let networkingClient = NetworkingClient()
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginUIButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginUIButton.layer.cornerRadius = 12
        
    }
    
    private func navigateToMainInterface(){
        
        let mainStoryboard = UIStoryboard(name: "main", bundle: Bundle.main)
        
        guard let mainNavigationVC = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as? MainNavigationController else {
            return
        }
        
        mainNavigationVC.modalPresentationStyle = .fullScreen
        present(mainNavigationVC, animated: true, completion: nil)
        
    }
    
    
    
    
}

extension LoginViewController : UITextFieldDelegate{
    
    @IBAction func loginButton(_ sender: Any) {
        // did not handle security with keychain wrapper
        
        userName = userNameTextField.text!
        password = passwordTextField.text!
        
        guard let urlToExecute = URL(string: "https://playground.tesonet.lt/v1/tokens?username=\(userName)&password=\(password)") else {
            return
        }
        
        networkingClient.executeAuth(urlToExecute) { (succes, error) in
            
            if succes == true {
                DispatchQueue.main.async {
                    self.navigateToMainInterface()
                }
            }
            else if succes == false {
                
                //  did not handle validation correctly
                //
                //        userNameTextField.attributedPlaceholder = NSAttributedString(string: "something went wrong..",
                //                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(displayP3Red: 0.4, green: 0.0, blue: 0.4, alpha: 1)])
            }
        }
        
        userNameTextField.text = ""
        passwordTextField.text = ""
        
    }
    
}











