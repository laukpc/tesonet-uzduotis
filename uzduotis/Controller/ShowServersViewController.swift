//
//  landingViewController.swift
//  uzduotis
//
//  Created by Laurynas Kapacinskas on 2020-08-14.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//

import UIKit

class ShowServersViewController: UIViewController {
    
    var dataToShow  : [ServerData]?
    var error : Error?
    let networkingClient = NetworkingClient()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkingClient.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
       
        self.tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "CustomTableViewCell")
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.allowsSelection = true
        self.tableView.separatorInset = .init(top: 0, left: 15, bottom: 0, right: 15)
        
        networkingClient.executeAuthRequest()
    }
    

}


extension ShowServersViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToShow?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell
        
        cell.countryName.text = dataToShow![indexPath.row].name
        cell.distance.text = String("\(dataToShow![indexPath.row].distance) km")
        cell.buttonImage = UIImage(named: "database.png")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}



extension ShowServersViewController : ServerDataDelegate {

    func serverDataUpdate(_ data: [ServerData]) {
        // updates data when request is executed
        // didn't handle errors properly here also
    dataToShow = data
}

}
