//
//  HomeScreenViewController.swift
//  uzduotis
//
//  Created by Laurynas Kapacinskas on 2020-08-16.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController {

    let showServersVC = ShowServersViewController()
    
    private let networkingClient = NetworkingClient()
    
    @IBOutlet weak var goToServersButton: UIButton!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        goToServersButton.layer.cornerRadius = 35
        logOutButton.layer.cornerRadius = 10
       
    }
    
    
    @IBAction func logOutAction(_ sender: Any) {
        
      navigateToLoginScreen()
    }
    
    @IBAction func showServers(_ sender: Any) {
     // intended to execute https request for servers here, but couldn't pass data to next VC
        
    }
    
    private func navigateToLoginScreen(){
         
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: true, completion: nil)

    }
}



    
    

