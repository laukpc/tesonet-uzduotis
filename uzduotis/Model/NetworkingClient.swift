//
//  RequestsManager.swift
//  uzduotis
//
//  Created by Laurynas Kapacinskas on 2020-08-16.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

protocol ServerDataDelegate {
    func serverDataUpdate ( _ data: [ServerData])
}

class NetworkingClient{
    
    
    typealias WebServiceResponse = (Bool, Error?) -> Void
    // tried to use Alamofire but had struggles with headers
    
    // handle authentication and store token
    func executeAuth(_ url: URL, completion: @escaping WebServiceResponse) {
        let semaphore = DispatchSemaphore (value: 0)
        
        var request = URLRequest(url: url,timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        // does not produce errors
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                let succes = false
                let error = error
                completion(succes, error)
                return
            }
            if let token = self.parseAuth(data){
                let tokenValue: Bool = KeychainWrapper.standard.set(token, forKey: "accessToken")
                completion(tokenValue, nil)
                
            }
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
    }
    
    
    
    
    // execute authorized requests with token
    
    var delegate : ServerDataDelegate?
    func executeAuthRequest(){
        let tokenValue : String? = KeychainWrapper.standard.string(forKey: "accessToken")
        
        if tokenValue != nil {
            let semaphore = DispatchSemaphore (value: 0)
            
            var request = URLRequest(url: URL(string: "https://playground.tesonet.lt/v1/servers")!,timeoutInterval: Double.infinity)
            request.addValue("Bearer \(tokenValue!)", forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else {
                    print(String(describing: error))
                    return
                }
                
                if let serverData = self.parseServerDataJSON(data){
                    
                    self.delegate?.serverDataUpdate(serverData)
                    //print(serverData)
                    
                }
                semaphore.signal()
            }
            
            task.resume()
            semaphore.wait()
           
        } else {
            return
        }
    }
    
    
    func parseServerDataJSON (_ serverData : Data) -> [ServerData]? {
        
        do{
            let servers: [ServerData] = try JSONDecoder().decode([ServerData].self, from: serverData)
            print(servers.count)
            return servers
        }
        catch {
            return nil
        }
    }
    
    
    func parseAuth (_ authData : Data ) -> String? {
        
        do{
            let accessToken : AccessToken? = try JSONDecoder().decode(AccessToken.self, from: authData)
            return accessToken?.token
            
        }
        catch{
            return nil
        }
    }
    
    
}
