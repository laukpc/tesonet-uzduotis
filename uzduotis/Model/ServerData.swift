//
//  RequestsData.swift
//  uzduotis
//
//  Created by Laurynas Kapacinskas on 2020-08-16.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//

import Foundation

struct ServerData : Decodable {
    let name : String
    let distance : Int
}

struct AccessToken : Decodable {
    let token : String
}
