//
//  CustomTableViewCell.swift
//  uzduotis
//
//  Created by Laurynas Kapacinskas on 2020-08-17.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//
import UIKit
import SnapKit

class CustomTableViewCell: UITableViewCell {
    
    var buttonTitle : String?
    var buttonImage : UIImage?
    var distanceText : Int?
    // country name text view
    var countryName : UITextView = {
        
        var countryNameText = UITextView()
        countryNameText.translatesAutoresizingMaskIntoConstraints = false
        countryNameText.isEditable = false
        countryNameText.isSelectable = false
        countryNameText.isUserInteractionEnabled = false
        countryNameText.isScrollEnabled = false
        countryNameText.backgroundColor = nil
        countryNameText.font = UIFont.systemFont(ofSize: 15)
        return countryNameText
        
    }()
    // distance text view
    var distance : UITextView = {
        
        var distanceText = UITextView()
        distanceText.translatesAutoresizingMaskIntoConstraints = false
        distanceText.isEditable = false
        distanceText.isSelectable = false
        distanceText.isUserInteractionEnabled = false
        distanceText.isScrollEnabled = false
        distanceText.backgroundColor = nil
        distanceText.font = UIFont.systemFont(ofSize: 15)
        return distanceText
    }()
    // image view
    var buttonImageView : UIImageView = {
        
        var buttonImageView = UIImageView()
        buttonImageView.translatesAutoresizingMaskIntoConstraints = false
        return buttonImageView
    }()
    
    // constraints
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let maxWidthContainer: CGFloat = 51.2
        let maxHeightContainer: CGFloat = 51.2
        
        super.addSubview(countryName)
        super.addSubview(buttonImageView)
        super.addSubview(distance)
        
        
        countryName.snp.makeConstraints { (make) in
            //center the imageView relative of superView
            make.center.equalToSuperview()
        }
        
        buttonImageView.snp.makeConstraints { (make) in
            // centered X and Y
            make.centerY.equalToSuperview()
            
            // left anchor to the left of separatorInset
            make.left.equalTo(super.separatorInset.left)
            
            // leading and top >= 20
            make.leading.top.greaterThanOrEqualTo(20)
            
            // trailing and bottom <= 20
            make.bottom.lessThanOrEqualTo(20)
            
            // width ratio to height
            make.width.equalTo(buttonImageView.snp.height).multipliedBy(maxWidthContainer/maxHeightContainer)
            
            // this will make it as tall and wide as possible, until it violates another constraint
            make.width.height.equalToSuperview().priority(.high)
            
            // max height
            make.height.lessThanOrEqualTo(maxHeightContainer)
            
        }
        
        distance.snp.makeConstraints { (make) in
            // did not handled to allign to separatorInset.right
            make.centerY.equalToSuperview()
            
            make.right.greaterThanOrEqualToSuperview()
            make.rightMargin.lessThanOrEqualTo(super.separatorInset.right)
        
            
        }
        
    
        
 
        
        
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        if let buttonTitle = buttonTitle {
            countryName.text = buttonTitle
            
        }
        if let buttonImage = buttonImage {
            buttonImageView.image = buttonImage
        }
        if let distanceText = distanceText {
            distance.text = String(distanceText)
        }
        
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


